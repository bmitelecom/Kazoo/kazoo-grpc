# kazoo-grpc a new way to communicate with Kazoo API
This project is aimed to provide a gRPC API point for a Kazoo API.
It might me useful for micro-service applications, since gRPC became ubiquitous.

# protobuf definition
The main .proto file is placed in **protobuf** directory. Later it might be separated to smaller parts or even packages
since it's not very convenient to use one flat list of messages.
