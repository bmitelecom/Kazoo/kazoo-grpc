package kazooapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	//kazoo "kazoo-go"

	proto "kazoo-grpc/protobuf"
	"net/url"

	"github.com/golang/protobuf/jsonpb"
	"golang.org/x/net/context"
)

// Linger please
var (
	_ context.Context
)

type AccountApiService service

/*
//Account represents a Kazoo account
type Account struct {
	Name        string                 `json:"name,omitempty"`
	Org         string                 `json:"org,omitempty"`
	Enabled     bool                   `json:"enabled,omitempty"`
	Language    string                 `json:"language,omitempty"`
	Timezone    string                 `json:"timezone,omitempty"`
	Recording   CallRecording          `json:"call_recording,omitempty"`
	Restriction map[string]interface{} `json:"call_restriction,omitempty"`
	CallerID    map[string]interface{} `json:"caller_id,omitempty"`
	Preflow     map[string]interface{} `json:"preflow,omitempty"`
	Metaflow    map[string]interface{} `json:"metaflow,omitempty"`
}*/

//Descendant represents a structure of an descendant
type Descendant struct {
	ID    string   `json:"id,omitempty"`
	Name  string   `json:"name,omitempty"`
	Realm string   `json:"realm,omitempty"`
	Tree  []string `json:"tree,omitempty"`
}

//CallRecording represents settings of call recording feature
type CallRecording struct {
	Account  CallRecordingEndpoint `json:"account,omitempty"`
	Endpoint CallRecordingEndpoint `json:"endpoint,omitempty"`
}

//CallRecordingEndpoint defines the direction of call-recording
type CallRecordingEndpoint struct {
	Any      CallRecordingSourse `json:"any,omitempty"`
	Inbound  CallRecordingSourse `json:"inbound,omitempty"`
	Outbound CallRecordingSourse `json:"outbound,omitempty"`
}

//CallRecordingSourse defines a source of call-recording
type CallRecordingSourse struct {
	Any    CallRecordingParameters `json:"any,omitempty"`
	Offnet CallRecordingParameters `json:"offnet,omitempty"`
	Onnet  CallRecordingParameters `json:"onnet,omitempty"`
}

//CallRecordingParameters defines a set of parameters of call-recording
type CallRecordingParameters struct {
	Enabled          bool   `json:"enabled,omitempty"`
	Format           string `json:"format,omitempty"`
	RecordMinSec     int    `json:"record_min_sec,omitempty"`
	RecordOnAnswer   bool   `json:"record_on_answer,omitempty"`
	RecordOnBridge   bool   `json:"record_on_bridge,omitempty"`
	RecordSampleRate bool   `json:"record_sample_rate,omitempty"`
	TimeLimit        int32  `json:"time_limit,omitempty"`
	URL              string `json:"url,omitempty"`
}

//Descendants hold a list of all descendants
type Descendants []Descendant

func (a *AccountApiService) GetAccount(ctx context.Context, account string) (acc *proto.Account, err error) {
	localVarHttpMethod := "GET"

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/accounts/" + account

	r, err := a.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	env := proto.AccountData{}

	decoder := json.NewDecoder(localVarHttpResponse.Body)

	um := jsonpb.Unmarshaler{AllowUnknownFields: true}
	umErr := um.UnmarshalNext(decoder, &env)
	if umErr != nil {
		return nil, umErr
	}

	acc = env.Data

	return acc, err
}

func (a *AccountApiService) GetAccountDescendants(ctx context.Context, account string) (*Descendants, error) {
	var (
		localVarHttpMethod = "GET"
		localVarPostBody   interface{}
		localVarFileName   string
		localVarFileBytes  []byte
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/accounts/" + account + "/descendants"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// body params
	//localVarPostBody = &body
	r, err := a.client.prepareRequest(
		ctx,
		localVarPath,
		localVarHttpMethod,
		localVarPostBody,
		localVarHeaderParams,
		localVarQueryParams,
		localVarFormParams,
		localVarFileName,
		localVarFileBytes)

	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	return nil, nil
}

//CreateAccount makes a new Kazoo account
func (a *AccountApiService) CreateAccount(ctx context.Context, account *proto.Account) (acc *proto.Account, err error) {
	var (
		localVarHttpMethod = "PUT"
		localVarPostBody   interface{}
		localVarFileName   string
		localVarFileBytes  []byte
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/accounts"
	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	data := proto.AccountData{Data: account}

	marshaller := jsonpb.Marshaler{
		EmitDefaults: false,
	}
	jsonStr, marErr := marshaller.MarshalToString(&data)
	//marErr := marshaller.Marshal(localJSONBuffer, &data)
	if marErr != nil {
		return nil, marErr
	}

	//localVarPostBody, bodyErr := setBody(localJSONBuffer, "json")
	localVarPostBody, bodyErr := setBody(jsonStr, "json")
	if bodyErr != nil {
		return nil, bodyErr
	}

	r, err := a.client.prepareRequest(
		ctx,
		localVarPath,
		localVarHttpMethod,
		localVarPostBody,
		localVarHeaderParams,
		localVarQueryParams,
		localVarFormParams,
		localVarFileName,
		localVarFileBytes)

	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 201:
		env := proto.AccountData{}

		decoder := json.NewDecoder(localVarHttpResponse.Body)

		um := jsonpb.Unmarshaler{AllowUnknownFields: true}
		umErr := um.UnmarshalNext(decoder, &env)
		if umErr != nil {
			fmt.Println(umErr)
			return nil, umErr
		}

		acc = env.Data

		return acc, err
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}
}

//CreateAccount makes a new child Kazoo account
func (a *AccountApiService) CreateChildAccount(ctx context.Context, caccount *proto.ChildAccount) (acc *proto.Account, err error) {
	var (
		localVarHttpMethod = "PUT"
		localVarPostBody   interface{}
		localVarFileName   string
		localVarFileBytes  []byte
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/accounts/" + caccount.ParentAccountId
	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	data := proto.AccountData{Data: caccount.Account}

	marshaller := jsonpb.Marshaler{
		EmitDefaults: false,
	}
	jsonStr, marErr := marshaller.MarshalToString(&data)
	//marErr := marshaller.Marshal(localJSONBuffer, &data)
	if marErr != nil {
		return nil, marErr
	}

	//localVarPostBody, bodyErr := setBody(localJSONBuffer, "json")
	localVarPostBody, bodyErr := setBody(jsonStr, "json")
	if bodyErr != nil {
		return nil, bodyErr
	}

	r, err := a.client.prepareRequest(
		ctx,
		localVarPath,
		localVarHttpMethod,
		localVarPostBody,
		localVarHeaderParams,
		localVarQueryParams,
		localVarFormParams,
		localVarFileName,
		localVarFileBytes)

	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 201:
		env := proto.AccountData{}

		decoder := json.NewDecoder(localVarHttpResponse.Body)

		um := jsonpb.Unmarshaler{AllowUnknownFields: true}
		umErr := um.UnmarshalNext(decoder, &env)
		if umErr != nil {
			fmt.Println(umErr)
			return nil, umErr
		}

		acc = env.Data

		return acc, err
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}
}

func (a *AccountApiService) ListSiblings(ctx context.Context, sibling string) (sblngs []*proto.Sibling, err error) {
	localVarHttpMethod := "GET"

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/accounts/" + sibling + "/siblings?paginate=false"

	r, err := a.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	env := proto.ListSiblings{}

	decoder := json.NewDecoder(localVarHttpResponse.Body)

	um := jsonpb.Unmarshaler{AllowUnknownFields: true}
	umErr := um.UnmarshalNext(decoder, &env)
	if umErr != nil {
		fmt.Println(umErr)
		return nil, umErr
	}

	sblngs = env.Data

	return sblngs, err
}

func (a *AccountApiService) ListDescendants(ctx context.Context, ancestor string) (descendants []*proto.Sibling, err error) {
	localVarHttpMethod := "GET"

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/accounts/" + ancestor + "/descendants?paginate=false"

	r, err := a.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	env := proto.ListSiblings{}

	decoder := json.NewDecoder(localVarHttpResponse.Body)

	um := jsonpb.Unmarshaler{AllowUnknownFields: true}
	umErr := um.UnmarshalNext(decoder, &env)
	if umErr != nil {
		fmt.Println(umErr)
		return nil, umErr
	}

	descendants = env.Data

	return descendants, err
}

func (a *AccountApiService) DeleteAccount(ctx context.Context, account string) (acc *proto.Account, err error) {
	localVarHttpMethod := "DELETE"

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/accounts/" + account

	r, err := a.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 200:
		env := proto.AccountData{}

		decoder := json.NewDecoder(localVarHttpResponse.Body)

		um := jsonpb.Unmarshaler{AllowUnknownFields: true}
		umErr := um.UnmarshalNext(decoder, &env)
		if umErr != nil {
			fmt.Println(umErr)
			return nil, umErr
		}

		acc = env.Data

		return acc, err
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}
}
