package main

import (
	"context"
	"errors"
	kazooapi "kazoo-grpc/api"
	proto "kazoo-grpc/protobuf"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var kz kazooapi.APIClient

// server is used to implement helloworld.GreeterServer.
type server struct{}

func (s *server) GetAccount(ctx context.Context, a *proto.Account) (acc *proto.Account, err error) {
	if a.Id == "" {
		return nil, errors.New("You have to provide ID of account in the protobuf message")
	}

	acc, err = kz.AccountAPI.GetAccount(ctx, a.Id)
	if err != nil {
		return nil, err
	}

	return acc, nil
}

func (s *server) CreateAccount(ctx context.Context, a *proto.Account) (acc *proto.Account, err error) {
	if a.Name == "" {
		return nil, errors.New("You have to provide the Name for a new account")
	}

	if a.Id != "" {
		return nil, errors.New("You mustn't provide the ID for a new account")
	}

	acc, err = kz.AccountAPI.CreateAccount(ctx, a)
	if err != nil {
		return nil, err
	}

	return acc, err
}

func (s *server) CreateChildAccount(ctx context.Context, a *proto.ChildAccount) (acc *proto.Account, err error) {
	if a.ParentAccountId == "" {
		return nil, errors.New("You have to provide the ID of parent account")
	}

	if a.ParentAccountId == a.Account.Id {
		return nil, errors.New("The ID of parent account mustn't be the same as the ID of account")
	}

	if a.Account.Name == "" {
		return nil, errors.New("You have to provide the Name for a new account")
	}

	acc, err = kz.AccountAPI.CreateChildAccount(ctx, a)
	if err != nil {
		return nil, err
	}

	return acc, err
}

func (s *server) ListSiblings(a *proto.Sibling, stream proto.Kazoo_ListSiblingsServer) error {
	ctx := context.Background()
	if a.Id == "" {
		return errors.New("You have to provide the ID of the account for which you request siblings")
	}

	list, err := kz.AccountAPI.ListSiblings(ctx, a.Id)
	if err != nil {
		Error.Println(err)
		return err
	}

	for _, dp := range list {
		if err := stream.Send(dp); err != nil {
			Error.Println(err)
			return err
		}
	}

	return nil
}

func (s *server) ListDescendants(a *proto.Sibling, stream proto.Kazoo_ListDescendantsServer) error {
	ctx := context.Background()
	if a.Id == "" {
		return errors.New("You have to provide the ID of the account for which you request descendants")
	}

	list, err := kz.AccountAPI.ListDescendants(ctx, a.Id)
	if err != nil {
		Error.Println(err)
		return err
	}

	for _, dp := range list {
		if err := stream.Send(dp); err != nil {
			Error.Println(err)
			return err
		}
	}

	return nil
}

func (s *server) DeleteAccount(ctx context.Context, a *proto.Account) (acc *proto.Account, err error) {
	if a.Id == "" {
		return nil, errors.New("You have to provide ID of the account")
	}

	acc, err = kz.AccountAPI.DeleteAccount(ctx, a.Id)
	if err != nil {
		return nil, err
	}

	return acc, err
}

func (s *server) GetPhoneNumber(ctx context.Context, n *proto.PhoneNumber) (num *proto.PhoneNumber, err error) {
	if n.Account == "" {
		return nil, errors.New("You have to provide ID of account in the protobuf message")
	}

	if n.Id == "" || len(n.Id) < 8 || len(n.Id) > 15 {
		return nil, errors.New("You have to provide a valid phone number")
	}

	num, err = kz.PhoneNumbersAPI.GetPhoneNumber(ctx, n.Account, n.Id)
	if err != nil {
		return nil, err
	}

	return num, err
}

func (s *server) CreatePhoneNumber(ctx context.Context, n *proto.PhoneNumber) (num *proto.PhoneNumber, err error) {
	if n.Account == "" {
		return nil, errors.New("You have to provide ID of account in the protobuf message")
	}

	if n.Id == "" || len(n.Id) < 8 || len(n.Id) > 15 {
		return nil, errors.New("You have to provide a valid phone number")
	}

	num, err = kz.PhoneNumbersAPI.CreatePhoneNumber(ctx, n.Account, n.Id)
	if err != nil {
		return nil, err
	}

	return num, err
}

func (s *server) DeletePhoneNumber(ctx context.Context, n *proto.PhoneNumber) (num *proto.PhoneNumber, err error) {
	if n.Account == "" {
		return nil, errors.New("You have to provide ID of account in the protobuf message")
	}

	if n.Id == "" || len(n.Id) < 8 || len(n.Id) > 15 {
		return nil, errors.New("You have to provide a valid phone number")
	}

	num, err = kz.PhoneNumbersAPI.DeletePhoneNumber(ctx, n.Account, n.Id)
	if err != nil {
		return nil, err
	}

	return num, err
}

func (s *server) GetUser(ctx context.Context, u *proto.User) (usr *proto.User, err error) {
	if u.Id == "" {
		return nil, errors.New("You have to provide ID of the user in the protobuf message")
	}

	if u.AccountId == "" {
		return nil, errors.New("You have to provide ID of the account the user belongs to")
	}

	usr, err = kz.UserAPI.GetUser(ctx, u.AccountId, u.Id)

	if err != nil {
		return nil, err
	}

	return usr, nil
}

func (s *server) ListUsers(u *proto.User, stream proto.Kazoo_ListUsersServer) error {
	ctx := context.Background()
	if u.AccountId == "" {
		return errors.New("You have to provide ID of the account the user belongs to")
	}

	list, err := kz.UserAPI.ListUsers(ctx, u.AccountId)
	if err != nil {
		Error.Println(err)
		return err
	}

	for _, dp := range list {
		if err := stream.Send(dp); err != nil {
			Error.Println(err)
			return err
		}
	}

	return nil
}

func (s *server) CreateUser(ctx context.Context, u *proto.User) (usr *proto.User, err error) {
	if u.AccountId == "" {
		return nil, errors.New("You have to provide ID of the account this user belongs to")
	}

	if u.FirstName == "" {
		return nil, errors.New("You have to provide a valid First Name")
	}

	if u.LastName == "" {
		return nil, errors.New("You have to provide a valid Last Name")
	}

	usr, err = kz.UserAPI.CreateUser(ctx, u)
	if err != nil {
		return nil, err
	}

	return usr, err
}

func (s *server) GetDevice(ctx context.Context, d *proto.Device) (dev *proto.Device, err error) {
	if d.Id == "" {
		return nil, errors.New("You have to provide ID of the device in the protobuf message")
	}

	if d.AccountId == "" {
		return nil, errors.New("You have to provide ID of the account the device belongs to")
	}

	dev, err = kz.DeviceAPI.GetDevice(ctx, d.AccountId, d.Id)

	if err != nil {
		return nil, err
	}

	return dev, nil
}

func (s *server) ListDevices(d *proto.Device, stream proto.Kazoo_ListDevicesServer) error {
	ctx := context.Background()
	if d.AccountId == "" {
		return errors.New("You have to provide ID of the account devices belongs to")
	}

	list, err := kz.DeviceAPI.ListDevices(ctx, d.AccountId)
	if err != nil {
		Error.Println(err)
		return err
	}

	for _, dp := range list {
		if err := stream.Send(dp); err != nil {
			Error.Println(err)
			return err
		}
	}

	return nil
}

func (s *server) CreateDevice(ctx context.Context, d *proto.Device) (dev *proto.Device, err error) {
	if d.AccountId == "" {
		return nil, errors.New("You have to provide ID of the account this device belongs to")
	}

	if d.Name == "" {
		return nil, errors.New("You have to provide a valid Name for the device")
	}

	dev, err = kz.DeviceAPI.CreateDevice(ctx, d)
	if err != nil {
		return nil, err
	}

	return dev, err
}

func (s *server) DeleteDevice(ctx context.Context, d *proto.Device) (dev *proto.Device, err error) {
	if d.AccountId == "" {
		return nil, errors.New("You have to provide ID of the account this device belongs to")
	}

	if d.Id == "" {
		return nil, errors.New("You have to provide ID of the device in the protobuf message")
	}

	dev, err = kz.DeviceAPI.DeleteDevice(ctx, d.AccountId, d.Id)
	if err != nil {
		return nil, err
	}

	return dev, err
}

func (s *server) ListDevicesRegistrationState(a *proto.Account, stream proto.Kazoo_ListDevicesRegistrationStateServer) error {
	ctx := context.Background()
	if a.Id == "" {
		return errors.New("You have to provide ID of the account devices belongs to")
	}

	list, err := kz.DeviceAPI.ListDevicesRegistrationState(ctx, a.Id)
	if err != nil {
		Error.Println(err)
		return err
	}

	for _, reg := range list {
		if err := stream.Send(reg); err != nil {
			Error.Println(err)
			return err
		}
	}

	return nil
}

func (s *server) RunSup(ctx context.Context, n *proto.Sup) (data *proto.SupData, err error) {
	if n.Command == "" {
		return nil, errors.New("You have to provide a SUP command")
	}

	if n.Parameter == "" {
		return nil, errors.New("You have to provide a valid SUP command parameter")
	}

	resp, respErr := kz.SupAPI.Sup(ctx, n.Command, n.Parameter, n.Subparameter)
	if respErr != nil {
		return nil, respErr
	}

	data = &proto.SupData{Data: resp}

	return data, err
}

func (s *server) GetAccountByName(ctx context.Context, a *proto.Account) (acc *proto.Account, err error) {
	if a.Name == "" {
		return nil, errors.New("You have to provide the name of the account you are looking for")
	}

	accID, err := kz.SupAPI.FindAccountIdByName(ctx, a.Name)
	if err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}

	acc, err = kz.AccountAPI.GetAccount(ctx, accID)
	if err != nil {
		return nil, err
	}

	return acc, nil

}

func (s *server) GetAccountByRealm(ctx context.Context, a *proto.Account) (acc *proto.Account, err error) {
	if a.Realm == "" {
		return nil, errors.New("You have to provide the realm of the account you are looking for")
	}

	accID, err := kz.SupAPI.FindAccountIdByRealm(ctx, a.Realm)
	if err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}

	acc, err = kz.AccountAPI.GetAccount(ctx, accID)
	if err != nil {
		return nil, err
	}

	return acc, nil

}

func (s *server) GetAccountByNumber(ctx context.Context, n *proto.PhoneNumber) (acc *proto.Account, err error) {
	if n.Id == "" {
		return nil, errors.New("You have to provide the ID of the phone number you are looking for")
	}

	if err != nil {
		return nil, err
	}

	accID, err := kz.SupAPI.FindAccountIdByNumber(ctx, n.Id)
	if err != nil {
		return nil, err
	}

	acc, err = kz.AccountAPI.GetAccount(ctx, accID)
	if err != nil {
		return nil, err
	}

	return acc, nil

}

func (s *server) GetRegistrationDetails(ctx context.Context, r *proto.RegistrationDetailsRequest) (det *proto.RegistrationStatus, err error) {
	if r.Username == "" {
		return nil, errors.New("You have to provide the credential record for a searching")
	}

	det, err = kz.SupAPI.GetRegistrarDetails(ctx, r.Username)
	if err != nil {
		return nil, err
	}

	return det, nil
}

func (s *server) ListRegistrationDetails(r *proto.RegistrationDetailsRequest, stream proto.Kazoo_ListRegistrationDetailsServer) error {
	var stateMap map[string]bool
	ctx := context.Background()
	if r.AccountId == "" {
		return errors.New("You have to provide the domain for which you are making the request")
	}

	acc, err := kz.AccountAPI.GetAccount(ctx, r.AccountId)
	if err != nil {
		return err
	}

	list, err := kz.DeviceAPI.ListDevices(ctx, r.AccountId)
	if err != nil {
		Error.Println(err)
		return err
	}

	states, err := kz.DeviceAPI.ListDevicesRegistrationState(ctx, r.AccountId)
	if err != nil {
		Error.Println(err)
		return err
	}

	stateMap = make(map[string]bool)

	for _, s := range states {
		stateMap[s.DeviceId] = s.Registered
	}

	for _, dp := range list {
		if dp.Enabled && stateMap[dp.Id] {
			d, err := kz.DeviceAPI.GetDevice(ctx, r.AccountId, dp.Id)
			if err != nil {
				Error.Println(err)
				return err
			}
			//0 - type "softphone"; 3 - type "sip_device"
			if d.DeviceType == 0 || d.DeviceType == 3 {
				req := d.Sip.Username + "@" + acc.Realm
				rd, err := kz.SupAPI.GetRegistrarDetails(ctx, req)
				if err != nil {
					Error.Println("Can't obtain registration details for", req, err)
					return err
				}
				if err := stream.Send(rd); err != nil {
					Error.Println(err)
					return err
				}

			}
		}
	}
	return nil
}

//========================================= STORAGE=================================================
func (s *server) GetStorage(ctx context.Context, sr *proto.StorageRequest) (stor *proto.Storage, err error) {
	if sr.AccountId == "" {
		return nil, errors.New("You have to provide an ID of the acccount you are requesting the storage for")
	}

	stor, err = kz.StorageAPI.GetStorage(ctx, sr.AccountId)

	if err != nil {
		return nil, err
	}

	return stor, nil
}

func (s *server) CreateStorage(ctx context.Context, sr *proto.NewStorageRequest) (stor *proto.Storage, err error) {
	if sr.AccountId == "" {
		return nil, errors.New("You have to provide an ID of the account this storage belongs to")
	}

	if sr.Data == nil {
		return nil, errors.New("You have to provide valid data for a new storage")
	}

	stor, err = kz.StorageAPI.CreateStorage(ctx, sr)
	if err != nil {
		return nil, err
	}

	return stor, err
}

func (s *server) DeleteStorage(ctx context.Context, sr *proto.StorageRequest) (stor *proto.Storage, err error) {
	if sr.AccountId == "" {
		return nil, errors.New("You have to provide an ID of the account this storage belongs to")
	}

	stor, err = kz.StorageAPI.DeleteStorage(ctx, sr.AccountId)
	if err != nil {
		return nil, err
	}

	return stor, err
}

func main() {
	Init()

	cfg := kazooapi.NewConfiguration()
	cfg.BasePath = config.KazooAPIURL
	cfg.APIKey = &kazooapi.APIKey{Key: config.KazooAPIKEY}
	clt, err := kazooapi.NewAPIClient(cfg)
	if err != nil {
		Error.Fatalf("failed to serve: %v", err)
	}
	kz = *clt

	lis, err := net.Listen("tcp", config.Address)
	if err != nil {
		Error.Fatalf("failed to listen: %v", err)
	}

	errHandler := func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		resp, err := handler(ctx, req)

		if err != nil {
			Error.Printf("Server: %s | Method %q Failed: %s", info.FullMethod, err)
		} else {
			Info.Printf("Server: %s | Method %q", info.Server, info.FullMethod)
		}
		return resp, err
	}

	s := grpc.NewServer(grpc.UnaryInterceptor(errHandler))

	proto.RegisterKazooServer(s, &server{})
	// Register reflection service on gRPC server.w
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		Error.Fatalf("failed to serve: %v", err)
	}
}
