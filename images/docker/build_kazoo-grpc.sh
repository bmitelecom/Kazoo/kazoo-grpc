#!/bin/bash -l

set -e
whereis go
git clone -b $BRANCH --single-branch --depth 1 \
    $GIT_URL kazoo-grpc
pushd $_
	#log::m-info "Applying patches ..."
	/usr/local/go/bin/go get -d -v
	/usr/local/go/bin/go env
	/usr/local/go/bin/go build

	chmod +x kazoo-grpc

echo $PWD
