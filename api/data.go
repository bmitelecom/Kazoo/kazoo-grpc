package kazooapi

//https://github.com/2600hz/kazoo/blob/master/applications/crossbar/doc/basics.md

//RequestEnvelope is the main structure which must be presented
//for each request contains body: POST,PUT
type RequestEnvelope struct {
	Data      interface{} `json:"data"`
	AuthToken string      `json:"auth_token,omitempty"` //optional
	Verb      string      `json:"verb,omitempty"`       //optional
}

//ResponseEnvelope is the main structure which must be presented
//for each request contains body: POST,PUT
type ResponseEnvelope struct {
	Data      interface{} `json:"data"`
	AuthToken string      `json:"auth_token,omitempty"`
	Status    string      `json:"status,omitempty"`     //one of "success", "error" or "fatal"
	Message   string      `json:"message,omitempty"`    //optional message that's should clarify
	Error     int         `json:"error,omitempty"`      //error code
	RequestID string      `json:"request_id,omitempty"` //for debugging purposes
	PageSize  int         `json:"page_size,omitempty"`
	Revision  string      `json:"revision"`
	Timestamp string      `json:"timestamp"`
	Version   string      `json:"version"`
	Node      string      `json:"node"`
}

//AuthResponse represents a Kazoo authentication record
type AuthResponse struct {
	PageSize  int                    `json:"page_size"`
	Data      map[string]interface{} `json:"data"`
	Revision  string                 `json:"revision"`
	Timestamp string                 `json:"timestamp"`
	Version   string                 `json:"version"`
	Node      string                 `json:"node"`
	RequestID string                 `json:"request_id"`
	Status    string                 `json:"status"`
	AuthToken string                 `json:"auth_token"`
}
