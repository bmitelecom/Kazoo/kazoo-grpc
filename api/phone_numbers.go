package kazooapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
	//kazoo "kazoo-go"

	proto "kazoo-grpc/protobuf"

	"github.com/golang/protobuf/jsonpb"
	"golang.org/x/net/context"
)

// Linger please
var (
	_ context.Context
)

type PhoneNumbersApiService service

func (a *PhoneNumbersApiService) GetPhoneNumber(ctx context.Context, acc, did string) (num *proto.PhoneNumber, err error) {
	localVarHttpMethod := "GET"

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/accounts/" + acc + "/phone_numbers/" + "%2B" + did

	r, err := a.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 200:
		env := proto.PhoneNumberData{}

		decoder := json.NewDecoder(localVarHttpResponse.Body)

		um := jsonpb.Unmarshaler{AllowUnknownFields: true}
		umErr := um.UnmarshalNext(decoder, &env)
		if umErr != nil {
			fmt.Println(umErr)
			return nil, umErr
		}

		num = env.Data

		return num, err
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}
}

func (a *PhoneNumbersApiService) CreatePhoneNumber(ctx context.Context, acc, did string) (num *proto.PhoneNumber, err error) {
	var (
		localVarFileName  string
		localVarFileBytes []byte
		localVarPostBody  interface{}
		//localJSONBuffer   bytes.Buffer
	)

	//It might be uncommented if necessary to pass viriable data as payload
	/*
		data := proto.PhoneNumberData{Data: {Id: did}}

		marshaller := jsonpb.Marshaler{
			EmitDefaults: true,
		}
		jsonStr, marErr := marshaller.MarshalToString(&data)
		//marErr := marshaller.Marshal(localJSONBuffer, &data)
		if marErr != nil {
			return nil, marErr
		}*/

	localVarHttpMethod := "PUT"
	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/accounts/" + acc + "/phone_numbers/" + did

	//localVarPostBody, bodyErr := setBody(localJSONBuffer, "json")
	/*localVarPostBody, bodyErr := setBody(jsonStr, "json")
	if bodyErr != nil {
		return nil, bodyErr
	}*/

	localVarPostBody = nil

	// create path and map variables
	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	localVarHeaderParams["Content-Type"] = "application/json"

	// body params
	//localVarPostBody = &authBody
	r, err := a.client.prepareRequest(
		ctx,
		localVarPath,
		localVarHttpMethod,
		localVarPostBody,
		localVarHeaderParams,
		localVarQueryParams,
		localVarFormParams,
		localVarFileName,
		localVarFileBytes)

	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 201:
		env := proto.PhoneNumberData{}

		decoder := json.NewDecoder(localVarHttpResponse.Body)

		um := jsonpb.Unmarshaler{AllowUnknownFields: true}
		umErr := um.UnmarshalNext(decoder, &env)
		if umErr != nil {
			fmt.Println(umErr)
			return nil, umErr
		}

		num = env.Data

		return num, err
	case 409:
		return nil, reportError("Status: %v, Body: Number %s already exists", localVarHttpResponse.Status, did)
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}
	return num, nil
}

func (a *PhoneNumbersApiService) DeletePhoneNumber(ctx context.Context, acc, did string) (num *proto.PhoneNumber, err error) {
	localVarHttpMethod := "DELETE"

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/accounts/" + acc + "/phone_numbers/" + did + "?hard=true"

	r, err := a.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 200:
		env := proto.PhoneNumberData{}

		decoder := json.NewDecoder(localVarHttpResponse.Body)

		um := jsonpb.Unmarshaler{AllowUnknownFields: true}
		umErr := um.UnmarshalNext(decoder, &env)
		if umErr != nil {
			fmt.Println(umErr)
			return nil, umErr
		}

		num = env.Data

		return num, err
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}
}
