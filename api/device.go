package kazooapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	//kazoo "kazoo-go"
	"net/url"

	proto "kazoo-grpc/protobuf"

	"github.com/golang/protobuf/jsonpb"
	"golang.org/x/net/context"
)

// Linger please
var (
	_ context.Context
)

type DeviceApiService service

func (d *DeviceApiService) GetDevice(ctx context.Context, account, device string) (dev *proto.Device, err error) {
	localVarHttpMethod := "GET"

	// create path and map variables
	localVarPath := d.client.cfg.BasePath + "/accounts/" + account + "/devices/" + device

	r, err := d.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := d.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	env := proto.DeviceData{}

	decoder := json.NewDecoder(localVarHttpResponse.Body)

	um := jsonpb.Unmarshaler{AllowUnknownFields: true}
	umErr := um.UnmarshalNext(decoder, &env)
	if umErr != nil {
		fmt.Println(umErr)
		return nil, umErr
	}

	dev = env.Data

	return dev, err
}

func (d *DeviceApiService) ListDevices(ctx context.Context, account string) (devcs []*proto.Device, err error) {
	localVarHttpMethod := "GET"

	// create path and map variables
	localVarPath := d.client.cfg.BasePath + "/accounts/" + account + "/devices"

	r, err := d.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := d.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	env := proto.ListDevices{}

	decoder := json.NewDecoder(localVarHttpResponse.Body)

	um := jsonpb.Unmarshaler{AllowUnknownFields: true}
	umErr := um.UnmarshalNext(decoder, &env)
	if umErr != nil {
		fmt.Println(umErr)
		return nil, umErr
	}

	devcs = env.Data

	return devcs, err
}

//CreateDevice makes a new Kazoo device
func (d *DeviceApiService) CreateDevice(ctx context.Context, device *proto.Device) (dev *proto.Device, err error) {
	var (
		localVarHttpMethod = "PUT"
		localVarPostBody   interface{}
		localVarFileName   string
		localVarFileBytes  []byte
	)

	// create path and map variables
	localVarPath := d.client.cfg.BasePath + "/accounts/" + device.AccountId + "/devices"
	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	data := proto.DeviceData{Data: device}

	marshaller := jsonpb.Marshaler{
		EmitDefaults: false,
	}
	jsonStr, marErr := marshaller.MarshalToString(&data)
	//marErr := marshaller.Marshal(localJSONBuffer, &data)
	if marErr != nil {
		return nil, marErr
	}

	//localVarPostBody, bodyErr := setBody(localJSONBuffer, "json")
	localVarPostBody, bodyErr := setBody(jsonStr, "json")
	if bodyErr != nil {
		return nil, bodyErr
	}

	// body params
	//localVarPostBody = &authBody
	r, err := d.client.prepareRequest(
		ctx,
		localVarPath,
		localVarHttpMethod,
		localVarPostBody,
		localVarHeaderParams,
		localVarQueryParams,
		localVarFormParams,
		localVarFileName,
		localVarFileBytes)

	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := d.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 201:
		env := proto.DeviceData{}

		decoder := json.NewDecoder(localVarHttpResponse.Body)

		um := jsonpb.Unmarshaler{AllowUnknownFields: true}
		umErr := um.UnmarshalNext(decoder, &env)
		if umErr != nil {
			fmt.Println(umErr)
			return nil, umErr
		}

		dev = env.Data

		return dev, err
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

}

func (d *DeviceApiService) DeleteDevice(ctx context.Context, account, device string) (dev *proto.Device, err error) {
	localVarHttpMethod := "DELETE"

	// create path and map variables
	localVarPath := d.client.cfg.BasePath + "/accounts/" + account + "/devices/" + device

	r, err := d.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := d.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 200:
		env := proto.DeviceData{}

		decoder := json.NewDecoder(localVarHttpResponse.Body)

		um := jsonpb.Unmarshaler{AllowUnknownFields: true}
		umErr := um.UnmarshalNext(decoder, &env)
		if umErr != nil {
			fmt.Println(umErr)
			return nil, umErr
		}

		dev = env.Data

		return dev, err
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}
}

func (d *DeviceApiService) ListDevicesRegistrationState(ctx context.Context, account string) (states []*proto.DeviceRegistrationState, err error) {
	localVarHttpMethod := "GET"

	// create path and map variables
	localVarPath := d.client.cfg.BasePath + "/accounts/" + account + "/devices/status"

	r, err := d.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := d.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	env := proto.ListDevicesRegistrationState{}

	decoder := json.NewDecoder(localVarHttpResponse.Body)

	um := jsonpb.Unmarshaler{AllowUnknownFields: true}
	umErr := um.UnmarshalNext(decoder, &env)
	if umErr != nil {
		fmt.Println(umErr)
		return nil, umErr
	}

	states = env.Data

	return states, err
}
