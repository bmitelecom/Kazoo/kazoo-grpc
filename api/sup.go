package kazooapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"regexp"
	//kazoo "kazoo-go"

	proto "kazoo-grpc/protobuf"

	"github.com/golang/protobuf/jsonpb"
	"golang.org/x/net/context"
)

// Linger please
var (
	_ context.Context
)

type SupApiService service

func (a *SupApiService) Sup(ctx context.Context, command, param, subparam string) (data string, err error) {
	var localVarPath string
	localVarHttpMethod := "GET"

	// create path and map variables
	if command == "" || param == "" {
		return "", reportError("You have to specify a command and a parameter")
	}

	if subparam == "" {
		localVarPath = a.client.cfg.BasePath + "/sup/" + command + "/" + param
	} else {
		localVarPath = a.client.cfg.BasePath + "/sup/" + command + "/" + param + "/" + subparam
	}

	r, err := a.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return "", err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return "", err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 200:
		env := proto.SupData{}

		decoder := json.NewDecoder(localVarHttpResponse.Body)

		um := jsonpb.Unmarshaler{AllowUnknownFields: true}
		umErr := um.UnmarshalNext(decoder, &env)
		if umErr != nil {
			return "", umErr
		}

		data = env.Data

		return data, err
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return "", reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}
}

func (a *SupApiService) FindAccountIdByName(ctx context.Context, name string) (acc string, err error) {
	var localVarPath string
	localVarHttpMethod := "GET"

	localVarPath = a.client.cfg.BasePath + "/sup/crossbar/find_account_by_name/" + name

	r, err := a.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return "", err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return "", err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 200:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		acc = findAccount(string(bodyBytes))
		if acc != "" {
			return acc, nil
		}
		return "", reportError("Can't find an account in response")
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return "", reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}
}

func (a *SupApiService) FindAccountIdByNumber(ctx context.Context, number string) (acc string, err error) {
	var localVarPath string
	localVarHttpMethod := "GET"

	localVarPath = a.client.cfg.BasePath + "/sup/crossbar/find_account_by_number/" + number

	r, err := a.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return "", err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return "", err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 200:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		acc = findAccount(string(bodyBytes))
		if acc != "" {
			return acc, nil
		}
		return "", reportError("Can't find an account in response")
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return "", reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}
}

func (a *SupApiService) FindAccountIdByRealm(ctx context.Context, realm string) (acc string, err error) {
	rawData, err := a.Sup(ctx, "crossbar", "find_account_by_realm", realm)
	if err != nil {
		return "", err
	}

	acc = findAccount(rawData)
	if acc != "" {
		return acc, nil
	}
	return "", reportError("Can't find an account in response")
}

func (a *SupApiService) GetRegistrarDetails(ctx context.Context, username string) (rs *proto.RegistrationStatus, err error) {
	var (
		det proto.RegistrationStatus
	)

	rawData, err := a.Sup(ctx, "ecallmgr", "registrar_details", username)
	if err != nil {
		return nil, err
	}

	var re = regexp.MustCompile(`(?mU:Username\s*:\s(?P<username>.*)\nRealm\s*:\s*(?P<realm>.*)\nUser-Agent\s*:\s(?P<user_agent>.*)\nCall-ID\s*:\s(?P<call_id>.*)\nFrom-User\s*:\s(?P<from_user>.*)\nFrom-Host\s*:\s(?P<from_host>.*)\nTo-User\s*:\s(?P<to_user>.*)\nTo-Host\s*:\s(?P<to_host>.*)\nEvent-Timestamp\s*:\s(?P<event_timestamp>.*)\nContact\s*:\s(?P<contact>.*)\nOriginal-Contact\s*:\s(?P<original_contact>.*)\nPrevious-Contact\s*:\s(?P<previous_contact>.*)\nProxy-Path\s*:\s(?P<proxy_path>.*)\nProxy-IP\s*:\s(?P<proxy_ip>.*)\nProxy-Port\s*:\s(?P<proxy_port>.*)\nProxy-Protocol\s*:\s(?P<proxy_protocol>.*)\nExpires\s*:\s(?P<expires>.*)\nAccount-ID\s*:\s(?P<accountid>.*)\nAccount-DB\s*:\s(?P<account_db>.*)\nAccount-Realm\s*:\s(?P<account_realm>.*)\nAccount-Name\s*:\s(?P<account_name>.*)\nAuthorizing-ID\s*:\s(?P<authorizing_id>.*)\nAuthorizing-Type\s*:\s(?P<authorizing_type>.*)\nSuppress-Unregister\s*:\s(?P<suppress_register>.*)\nRegister-Overwrite.*\s*:\s(?P<register_overwrite>.*)\n(?:Owner-ID\s*:\s(?P<owner_id>.*)\n)?(?:Presence-ID\s*:\s(?P<presence_id>.*)\n)?Registrar-Node\s*:\s(?P<registrar_node>.*)\nBridge-RURI\s*:\s(?P<bridge_ruri>.*)\nFirst-Registration\s*:\s(?P<first_registration>.*)\nInitial-Reg.*\s*:\s(?P<initial_registration>.*)\nLast-Registration\s*:\s(?P<last_registration>.*)\nSource-IP\s*:\s(?P<source_ip>.*)\nSource-Port\s*:\s(?P<source_port>.*)\n\n)`)

	match := re.MatchString(rawData)
	if match {
		n1 := re.SubexpNames()
		r2 := re.FindAllStringSubmatch(rawData, -1)[0]

		md := map[string]string{}
		for i, n := range r2 {
			fmt.Printf("%d. match='%s'\tname='%s'\n", i, n, n1[i])
			md[n1[i]] = n
		}

		det = proto.RegistrationStatus{
			Username:            md["username"],
			Realm:               md["realm"],
			UserAgent:           md["user_agent"],
			CallId:              md["call_id"],
			FromUser:            md["from_user"],
			FromHost:            md["from_host"],
			ToUser:              md["to_user"],
			ToHost:              md["to_host"],
			EventTimestamp:      md["event_timestamp"],
			Contact:             md["contact"],
			OriginalContact:     md["original_contact"],
			PreviousContact:     md["previous_contact"],
			ProxyPath:           md["proxy_path"],
			ProxyIp:             md["proxy_ip"],
			ProxyPort:           md["proxy_port"],
			ProxyProtocol:       md["proxy_protocol"],
			Expires:             md["expires"],
			AccountId:           md["account_id"],
			AccountDb:           md["account_db"],
			AccountRealm:        md["account_realm"],
			Accountname:         md["accountname"],
			AuthorizingId:       md["authorizing_id"],
			AuthorizingType:     md["authorizing_type"],
			SuppressUnregister:  md["suppress_unregister"],
			RegisterOverwrite:   md["register_overwrite"],
			OwnerId:             md["owner_id"],
			RegistrarNode:       md["registrar_node"],
			BridgeRuri:          md["bridge_ruri"],
			FirstRegistration:   md["first_registration"],
			InitialRegistration: md["initial_registration"],
			LastRegistration:    md["last_registration"],
			SourceIp:            md["source_ip"],
			SourcePort:          md["source_port"],
			PresenceId:          md["presence_id"],
		}
		return &det, nil

	}
	return nil, reportError("Can't obtain registration details")
}

//checkDID validates a DID number
func findAccount(data string) string {
	r := regexp.MustCompile(`([a-z0-9]{32})`)
	acc := r.FindString(data)
	if acc != "" {
		return acc
	}

	return ""
}
