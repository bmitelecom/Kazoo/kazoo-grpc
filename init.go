package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

var (
	//Trace is necessary for debugging purposes
	Trace *log.Logger
	//Info corresponds to INFO syslog level
	Info *log.Logger
	//Warning corresponds to WARN syslog level
	Warning *log.Logger
	//Error corresponds to ERR syslog level
	Error *log.Logger
)

var (
	config     Configuration
	configFile string
)

func Init() {
	fmt.Println("===== Server initialization =====")
	//Set up logging options
	traceHandle := ioutil.Discard
	infoHandle := os.Stdout
	warningHandle := os.Stdout
	errorHandle := os.Stderr

	Trace = log.New(traceHandle,
		"TRACE: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Info = log.New(infoHandle,
		"INFO: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Warning = log.New(warningHandle,
		"WARNING: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Error = log.New(errorHandle,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	//Parses command line options
	flag.StringVar(&configFile, "c", "config.yaml", "Please, specify the path to the config file")
	//flag.StringVar(&celeryBackend, "backend", "CELERY_BACKEND_HERE", "Put a celery backend URI here")
	//flag.StringVar(&verificationToken, "token", "YOUR_VERIFICATION_TOKEN_HERE", "Your Slash Verification Token")
	flag.Parse()

	//Loads the configuration from a file
	loadConfig()
}

//Configuration describes configuration params
type Configuration struct {
	Address          string   `yaml:"address"`
	ReadTimeout      int64    `yaml:"readTimeout"`
	WriteTimeout     int64    `yaml:"writeTimeout"`
	AdminUsers       []string `yaml:"adminUsers"`
	KazooAPIURL      string   `yaml:"kazooApiUrl"`
	KazooAPIKEY      string   `yaml:"kazooApiKey"`
	KazooGrpcAddress string   `yaml:"kazooGrpcAddress"`
	ContextTimeout   int64    `yaml:"contextTimeout"`
}

func loadConfig() {
	file, err := os.Open(configFile)
	if err != nil {
		Error.Printf("Cannot open config file: %s", err)
	}
	decoder := yaml.NewDecoder(file)
	config = Configuration{}
	err = decoder.Decode(&config)
	if err != nil {
		Error.Fatalf("Cannot get configuration from file: %s", err)
	}

	Info.Printf("%+v\n", config)

	defer file.Close()
}
