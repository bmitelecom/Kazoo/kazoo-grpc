package kazooapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"

	proto "kazoo-grpc/protobuf"

	"github.com/golang/protobuf/jsonpb"
	"golang.org/x/net/context"
)

// Linger please
var (
	_ context.Context
)

type StorageApiService service

func (s *StorageApiService) GetStorage(ctx context.Context, account string) (stor *proto.Storage, err error) {
	localVarHttpMethod := "GET"

	// create path and map variables
	localVarPath := s.client.cfg.BasePath + "/accounts/" + account + "/storage"

	r, err := s.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := s.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 200:
		env := proto.StorageData{}

		decoder := json.NewDecoder(localVarHttpResponse.Body)

		um := jsonpb.Unmarshaler{AllowUnknownFields: true}
		umErr := um.UnmarshalNext(decoder, &env)
		if umErr != nil {
			fmt.Println(umErr)
			return nil, umErr
		}

		stor = env.Data

		return stor, err
	case 404:
		return nil, reportError("Requested storage not found: %v", localVarHttpResponse.Status)
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}
}

//CreateDevice makes a new Kazoo device
func (s *StorageApiService) CreateStorage(ctx context.Context, storage *proto.NewStorageRequest) (stor *proto.Storage, err error) {
	var (
		localVarHttpMethod = "PUT"
		localVarPostBody   interface{}
		localVarFileName   string
		localVarFileBytes  []byte
	)

	// create path and map variables
	localVarPath := s.client.cfg.BasePath + "/accounts/" + storage.AccountId + "/storage"
	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	data := proto.StorageData{Data: storage.Data}

	marshaller := jsonpb.Marshaler{
		EmitDefaults: false,
	}
	jsonStr, marErr := marshaller.MarshalToString(&data)
	//marErr := marshaller.Marshal(localJSONBuffer, &data)
	if marErr != nil {
		return nil, marErr
	}

	//localVarPostBody, bodyErr := setBody(localJSONBuffer, "json")
	localVarPostBody, bodyErr := setBody(jsonStr, "json")
	if bodyErr != nil {
		return nil, bodyErr
	}

	// body params
	//localVarPostBody = &authBody
	r, err := s.client.prepareRequest(
		ctx,
		localVarPath,
		localVarHttpMethod,
		localVarPostBody,
		localVarHeaderParams,
		localVarQueryParams,
		localVarFormParams,
		localVarFileName,
		localVarFileBytes)

	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := s.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 201:
		env := proto.StorageData{}

		decoder := json.NewDecoder(localVarHttpResponse.Body)

		um := jsonpb.Unmarshaler{AllowUnknownFields: true}
		umErr := um.UnmarshalNext(decoder, &env)
		if umErr != nil {
			return nil, umErr
		}

		stor = env.Data

		return stor, err
	case 409:
		return nil, reportError("Storage already exists %v", localVarHttpResponse.Status)
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}
}

func (s *StorageApiService) DeleteStorage(ctx context.Context, account string) (stor *proto.Storage, err error) {
	localVarHttpMethod := "DELETE"

	// create path and map variables
	localVarPath := s.client.cfg.BasePath + "/accounts/" + account + "/storage"

	r, err := s.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := s.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	env := proto.StorageData{}

	decoder := json.NewDecoder(localVarHttpResponse.Body)

	um := jsonpb.Unmarshaler{AllowUnknownFields: true}
	umErr := um.UnmarshalNext(decoder, &env)
	if umErr != nil {
		fmt.Println(umErr)
		return nil, umErr
	}

	stor = env.Data

	return stor, err
}

func CreateStorageAttachmentAWS(bucket, key, secret string) (data *proto.StorageAttachment, err error) {
	if bucket == "" || key == "" || secret == "" {
		return nil, reportError("You have to provide valid AWS credentials and a bucket name")
	}
	data = &proto.StorageAttachment{
		Handler: "s3",
		Settings: &proto.StorageAttachment_Settings{
			Bucket: bucket,
			Key:    key,
			Secret: secret,
			Scheme: "https", //default value
		},
	}

	return data, nil
}
