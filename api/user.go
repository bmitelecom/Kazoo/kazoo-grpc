package kazooapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
	//kazoo "kazoo-go"

	proto "kazoo-grpc/protobuf"

	"github.com/golang/protobuf/jsonpb"
	"golang.org/x/net/context"
)

// Linger please
var (
	_ context.Context
)

type UserApiService service

func (u *UserApiService) GetUser(ctx context.Context, account, user string) (usr *proto.User, err error) {
	localVarHttpMethod := "GET"

	// create path and map variables
	localVarPath := u.client.cfg.BasePath + "/accounts/" + account + "/users/" + user

	r, err := u.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := u.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	env := proto.UserData{}

	decoder := json.NewDecoder(localVarHttpResponse.Body)

	um := jsonpb.Unmarshaler{AllowUnknownFields: true}
	umErr := um.UnmarshalNext(decoder, &env)
	if umErr != nil {
		fmt.Println(umErr)
		return nil, umErr
	}

	usr = env.Data
	fmt.Println(usr)

	return usr, err
}

func (u *UserApiService) ListUsers(ctx context.Context, account string) (usrs []*proto.User, err error) {
	localVarHttpMethod := "GET"

	// create path and map variables
	localVarPath := u.client.cfg.BasePath + "/accounts/" + account + "/users"
	fmt.Println(localVarPath)

	r, err := u.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := u.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	env := proto.ListUsers{}

	decoder := json.NewDecoder(localVarHttpResponse.Body)

	um := jsonpb.Unmarshaler{AllowUnknownFields: true}
	umErr := um.UnmarshalNext(decoder, &env)
	if umErr != nil {
		fmt.Println(umErr)
		return nil, umErr
	}

	usrs = env.Data

	return usrs, err
}

//CreateAccount makes a new Kazoo account
func (u *UserApiService) CreateUser(ctx context.Context, user *proto.User) (usr *proto.User, err error) {
	var (
		localVarHttpMethod = "PUT"
		localVarPostBody   interface{}
		localVarFileName   string
		localVarFileBytes  []byte
	)

	// create path and map variables
	localVarPath := u.client.cfg.BasePath + "/accounts/" + user.AccountId + "/users"
	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	data := proto.UserData{Data: user}

	marshaller := jsonpb.Marshaler{
		EmitDefaults: false,
	}
	jsonStr, marErr := marshaller.MarshalToString(&data)
	//marErr := marshaller.Marshal(localJSONBuffer, &data)
	if marErr != nil {
		return nil, marErr
	}

	//localVarPostBody, bodyErr := setBody(localJSONBuffer, "json")
	localVarPostBody, bodyErr := setBody(jsonStr, "json")
	if bodyErr != nil {
		return nil, bodyErr
	}

	// body params
	//localVarPostBody = &authBody
	r, err := u.client.prepareRequest(
		ctx,
		localVarPath,
		localVarHttpMethod,
		localVarPostBody,
		localVarHeaderParams,
		localVarQueryParams,
		localVarFormParams,
		localVarFileName,
		localVarFileBytes)

	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := u.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	switch localVarHttpResponse.StatusCode {
	case 201:
		env := proto.UserData{}

		decoder := json.NewDecoder(localVarHttpResponse.Body)

		um := jsonpb.Unmarshaler{AllowUnknownFields: true}
		umErr := um.UnmarshalNext(decoder, &env)
		if umErr != nil {
			fmt.Println(umErr)
			return nil, umErr
		}

		usr = env.Data

		return usr, err
	default:
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

}
